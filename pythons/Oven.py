from KitchenAppliance import *
from DishesType import *

class Oven (KitchenAppliance):
    appliance_type = DishesType.CAKES
    def __init__(self,temperature,power,name,compatibility,dishes):
        self.temperature = temperature
        KitchenAppliance.__init__(self,power,name,compatibility,dishes)

    def __str__(self):
        return "Name : " + str(self.name) + " power: " + str(self.power) + " temperature is " + str(self.temperature)  + "  compatibility - " + str(self.compatibility)
