from KitchenAppliance import *
from DishesType import *

class Mixer (KitchenAppliance):
    appliance_type = DishesType.DESERTS
    def __init__(self,rotate,power,name,compatibility,dishes):
        self.rotate = rotate
        KitchenAppliance.__init__(self,power,name,compatibility,dishes)

    def __str__(self):
        return "Name : " + str(self.name) + " power: " + str(self.power) + " rotate is " + str(self.rotate) + "  compatibility - " + str(self.compatibility)
