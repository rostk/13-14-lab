from flask import Flask, jsonify, abort, make_response, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://root:38mefase@localhost:3306/test-db-py'
db = SQLAlchemy(app)


class KitchenAppliance(db.Model):
    __tablename__ = "kitchenapplience"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(45))
    dishes = db.Column(db.String(45))
    power = db.Column(db.Integer)
    powerConsumption = db.Column(db.Integer)r



@app.route('/appliance', methods=['GET'])
def get_all_appliace():
    appliance = []
    all_appliance = KitchenAppliance.query.all()
    for ap in all_appliance:
    db.session.commit()
    return jsonify({'appliance': appliance})


@app.route('/appliance/<int:kitchenAppliance_id>', methods=['GET'])
def get_kitchenAppliance(kitchenAppliance_id):
    ap = KitchenAppliance.query.filter_by(id=kitchenAppliance_id).first()
    kitchenAppliance = {
        'name': ap.name,
        'dishes': ap.dishes,
        'power': ap.power,
        'powerConsumption': ap.powerConsumption
    }
    db.session.commit()
    return jsonify({'kitchenAppliance': kitchenAppliance})


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/appliance', methods=['POST'])
def add_kitchenAppliance():
#    if not request.json or not 'name' in request.json:
#        abort(400)
    new_kitchenAppliance = KitchenAppliance()
    #new_kitchenAppliance.iD = request.json['id']
    new_kitchenAppliance.name = request.json['name']
    new_kitchenAppliance.dishes = request.json.get('dishes', "")
    new_kitchenAppliance.power = request.json.get('power', 0)
    new_kitchenAppliance.powerConsumption = request.json.get('powerConsumption', 0)

    db.session.add(new_kitchenAppliance)
    db.session.commit()
    return jsonify('Successful')


@app.route('/appliance/<int:kitchenAppliance_id>', methods=['PUT'])
def update_kitchenAppliance(kitchenAppliance_id):
    kitchenAppliance = KitchenAppliance.query.get(kitchenAppliance_id)

    kitchenAppliance.name = request.json['name']
    kitchenAppliance.dishes = request.json['dishes']
    kitchenAppliance.power = request.json.get('power', kitchenAppliance.power)
    kitchenAppliance.powerConsumption = request.json.get('powerConsumption', kitchenapplience.powerConsumption)
    db.session.commit()
    return jsonify('Successful')


@app.route('/appliance/<int:kitchenAppliance_id>', methods=['DELETE'])
def delete_kitchenAppliance(kitchenAppliance_id):
    ap = KitchenAppliance.query.filter_by(id=kitchenAppliance_id).first()
    db.session.delete(ap)
    db.session.commit()
    return jsonify({'result': True})


if __name__ == '__main__':
    app.run(debug=True)
